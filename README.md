## Questions for Quiz 1
By topic:
### Input, Output, Variables, Consts

1. What would be the output of the following code if for the input there would be `10` and `20`?
```
x = input("Enter a number: ")
y = input("Enter another number: ")
print(x + y)
```
- [ ] 30
- [ ] 10
- [ ] 20
- [x] 1020

2. What is the value of the variable `x` after the following code runs?

```
x = 10
y = 20
x = x + y
x = x * 2
print(x)
```

- [ ] 20
- [ ] 80
- [x] 60
- [ ] 40

3. What is the output of the following code?

```
x = 20
y = 30
z = x
x = y
y = z
print(x, y)
```

- [x] 30 20
- [ ] 20 20
- [ ] 20 30
- [ ] 30 30

4. What is the output of the following code?

```
x = 2
y = 3
z = x * y ** x
print(z)
```

- [ ] 36
- [X] 18
- [ ] 12
- [ ] Error

5. What is the output of the following code?

```
x = "hello"
y = "world"
z = x * 3 + y
```

- [ ] hello3world
- [ ] hello 3 world
- [ ] Error
- [x] hellohellohelloworld

### Conditionals, Functions

1. What will be the output of the following code?

```
def even_or_odd(x):
    if x % 2 == 0:
        print("Even")
    else:
        print("Odd")

print(even_or_odd(3 + 4))
```

- [X] Odd None
- [ ] Even
- [ ] Odd
- [ ] Error

2. What is the output of the following code?

```
def my_func(x):
    if x:
        return "True"
    else:
        return "False"

print(my_func(-1))
```

- [ ] False
- [ ] None
- [ ] Error
- [x] True

3. What will be the output of the following code?

```
def my_func(x):
    return 0
    if x > 5:
        return 1
    else:
        return 3
    return 2

print(my_func(5))
```

- [ ] 3
- [ ] 2
- [x] 0
- [ ] 1

4. What is the output of the following code?

```
def return_largest(a, b, c):
    if a > b and a > c:
        return a
    elif b > a and b > c:
        return b
    else:
        return c

print(return_largest(3, 3, 2))
```

- [X] 2
- [ ] 3
- [ ] None
- [ ] Error

5. What is the output of the following code?


```
def check_divisibility(x,y):
    if x % y == 0:
        return x/y
    else:
        return False

x = 0
y = check_divisibility(4,x)
print(y)
```

- [ ] False
- [ ] True
- [ ] 0
- [x] Error

### Loops, Recursion

1. What is the output of the following code?

```
def fibonacci(n):
    if n < 2:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)
        
print(fibonacci(6))
```

- [ ] 13
- [x] 8
- [ ] 6
- [ ] 5

2. What will be the output of the following code?

```
x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for i in range(len(x)):
    if x[i] % 2 == 0:
        x[i] = 'even'
print(x)
```

- [ ] even
- [ ] [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
- [X] [1, 'even', 3, 'even', 5, 'even', 7, 'even', 9, 'even']
- [ ] ['even', 'even', 'even', 'even', 'even']

3. What is the output of the following code?

```
x = [1,2,3,4,5]
for i in range(len(x)):
    if i % 2 == 0:
        print(x[i])
```

- [ ] 2, 4
- [x] 1, 3, 5
- [ ] 0, 2, 4
- [ ] None of the above

4. What will be the output of the following code?

```
x = [1, 2, 3, 4, 5]
i = 0
while i < len(x):
    if x[i] % 2 == 0:
        x[i] = x[i] * 2
    i += 1
print(x)
```

- [ ] [1, 2, 3, 4, 5]
- [ ] [2, 4, 6, 8, 10]
- [X] [1, 4, 3, 8, 5]
- [ ] None of the above

5. What will be the output of the following code?

```
x = [1, 2, 3, 4, 5]
i = 0
while i < len(x):
    if x[i] % 2 != 0:
        x[i] = x[i] * 2
    x[i] = x[i] * 2
    i += 1
print(x)
```

- [ ] [2, 4, 6, 8, 10]
- [ ] [1, 4, 3, 8, 5]
- [X] [4, 4, 12, 8, 20]
- [ ] None of the above

### Lists, Tuples, String

1. What is the output of the following code?

```
x = [1,2,3,4,5]
y = x
y[2] = 'hello'
print(y)
```

- [x] [1, 2, 'hello', 4, 5]
- [ ] [1, 'hello', 3, 4, 5]
- [ ] Error
- [ ] [1, 2, 'hello', 3, 4, 5]

2. What will be the output of the following code?

```
x = [1, 2, 3, 4, 5]
print(x[4:1:-1])
```

- [ ] [5, 1, 2]
- [x] [5, 4, 3]
- [ ] [2, 3, 4]
- [ ] Error

3. What will be the output of the following code?

```
x = (1, 2, 3, 4, 5)
y = x
y[2] = 2
```

- [ ] (1, 2, 2, 4, 5)
- [ ] (1, 2, 3, 4, 5)
- [x] Error
- [ ] None of the above

4. What will be the output of the following code?

```
x = "hello world"
y = x.split()
x = "".join(x)
print(x)
```

- [ ] hello world
- [ ] ["hello", "world"]
- [X] helloworld
- Error

5. What will be the output of the following code?

```
x = (1,2,3,4,5)
x = tuple(x)
x = x + (6,7)
print(x)
```

- [ ] Error
- [X] (1, 2, 3, 4, 5, 6, 7)
- [ ] ((1, 2, 3, 4, 5), (6, 7))
- [ ] None of the above

### Sets, Dictionaries

1. What will be the output of the following code?

```
x = {1,2,3,4,5}
y = {4,5,6,7,8}
print(x.intersection(y))
```

- [X] {4, 5}
- [ ] {1, 2, 3, 4, 5, 6, 7, 8}
- [ ] {}
- [ ] None

2. What will be the output of the following code?

```
x = {1: '5', 2: '4', 3: '3', 4: '2'}
x.pop(2)
print(x)
```

- [X] {1: '5', 3: '3', 4: '2'}
- [ ] {1: '5', 2: '4', 3: '3'}
- [ ] {1: '5', 2: '4', 3: '3', 4: '2'}
- [ ] Error

3. What is the output of the following code?

```
x = {1,2,3,4,5}
x.add(3)
print(x)
```

- [ ] {1,2,3,4,5,3}
- [X] {1,2,3,4,5}
- [ ] Error
- [ ] None of the above

4. What will be the output of the following code?

```
x = {1: 'a', 2: 'b', 3: 'c', 4: 'd'}
x.update({5: 'e'})
print(x)
```

- [ ] {1: 'a', 2: 'b', 3: 'c', 4: 'd'}
- [ ] {5: 'e'}
- [ ] Error
- [X] None of the above

5. What will be the output of the following code?

```
x = {1, 2, 3, 4, 5}
y = {4, 5, 6, 7, 8}
x.update(y)
print(x)
```

- [ ] {1, 2, 3, 4, 5}
- [ ] {1, 2, 3, 4, 5, 4, 5, 6, 7, 8}
- [X] {1, 2, 3, 4, 5, 6, 7, 8}
- [ ] {}
